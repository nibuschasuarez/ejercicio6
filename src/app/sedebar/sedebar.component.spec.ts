import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SedebarComponent } from './sedebar.component';

describe('SedebarComponent', () => {
  let component: SedebarComponent;
  let fixture: ComponentFixture<SedebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SedebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SedebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
