import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { INICIOComponent } from "./components/inicio/inicio.component";
import { GALERIAComponent } from "./components/galeria/galeria.component";
import { TEXTOComponent } from "./components/texto/texto.component";
import { PRODUCTOComponent } from "./components/producto/producto.component";
import {  OFERTASComponent} from "./components/ofertas/ofertas.component";
import { CONTACTOComponent } from './components/contacto/contacto.component';




const routes: Routes = [
  {path:'INICIO', component:INICIOComponent},
  {path:'GALERIA', component:GALERIAComponent},
  {path:'TEXTO', component:TEXTOComponent},
  {path:'PRODUCTOS',component:PRODUCTOComponent},
  {path:'OFERTAS',component:OFERTASComponent},
  {path: 'CONTACTO',component:CONTACTOComponent},

  {path:'**',pathMatch:'full', redirectTo:'INICIO'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
