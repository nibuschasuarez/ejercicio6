import { RouterModule, Routes } from "@angular/router";
import { GALERIAComponent } from "./components/galeria/galeria.component";
import { INICIOComponent } from "./components/inicio/inicio.component";
import { TEXTOComponent } from "./components/texto/texto.component";
import { PRODUCTOComponent } from "./components/producto/producto.component";
import {  OFERTASComponent} from "./components/ofertas/ofertas.component";




const rutas: Routes =[
  {path:'INICIO', component:INICIOComponent},
  {path:'GALERIA', component:GALERIAComponent},
  {path:'TEXTO', component:TEXTOComponent},
  {path:'PRODUCTOS',component:PRODUCTOComponent},
  {path:'OFERTAS',component:OFERTASComponent},
  {path:'**',pathMatch:'full', redirectTo:'INICIO'}
];

export const rutitas = RouterModule.forRoot(rutas);