import { ComponentFixture, TestBed } from '@angular/core/testing';

import { INICIO2Component } from './inicio2.component';

describe('INICIO2Component', () => {
  let component: INICIO2Component;
  let fixture: ComponentFixture<INICIO2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ INICIO2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(INICIO2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
