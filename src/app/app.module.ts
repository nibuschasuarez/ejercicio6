import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { rutitas } from './app.routes';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsComponent } from './components/components.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SedebarComponent } from './sedebar/sedebar.component';
import { INICIOComponent } from './components/inicio/inicio.component';
import { TEXTOComponent } from './components/texto/texto.component';
import { GALERIAComponent } from './components/galeria/galeria.component';
import { INICIO2Component } from './components/inicio2/inicio2.component';
import { PRODUCTOComponent } from './components/producto/producto.component';
import { CONTACTOComponent } from './components/contacto/contacto.component';
import { OFERTASComponent } from './components/ofertas/ofertas.component';


@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    NavbarComponent,
    SedebarComponent,
    INICIOComponent,
    TEXTOComponent,
    GALERIAComponent,
    INICIO2Component,
    PRODUCTOComponent,
    CONTACTOComponent,
    OFERTASComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
